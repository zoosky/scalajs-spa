lazy val server = (project in file("server"))
  .settings(commonSettings: _*)
  .settings(
    scalaJSProjects := Seq(client),

    pipelineStages in Assets := Seq(scalaJSPipeline),

    // triggers scalaJSPipeline when using compile or continuous compilation
    compile in Compile := ((compile in Compile) dependsOn scalaJSPipeline).value,

    libraryDependencies ++= Seq(
      "ch.qos.logback"     % "logback-classic" % "1.2.3",
      "com.lihaoyi"       %% "scalatags"       % "0.6.7",
      "com.typesafe.akka" %% "akka-http"       % "10.0.10"
    ),

    WebKeys.packagePrefix in Assets := "public/",

    managedClasspath in Runtime += (packageBin in Assets).value
  )
  .enablePlugins(SbtWeb)
  .dependsOn(sharedJVM)

lazy val client = (project in file("client"))
  .enablePlugins(ScalaJSPlugin)
  .settings(commonSettings: _*)
  .settings(
    scalaJSUseMainModuleInitializer := true,

    libraryDependencies ++= Seq(
      "com.github.japgolly.scalacss" %%% "core"         % "0.5.3",
      "com.lihaoyi"                  %%% "upickle"      % "0.4.4",
      "com.typesafe.play"            %%% "play-json"    % "2.6.7",
      "in.nvilla"                    %%% "monadic-html" % "0.4.0-RC1"
    )
  )
  .enablePlugins(ScalaJSPlugin, ScalaJSWeb)
  .dependsOn(sharedJS)

lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared"))
    .settings(commonSettings: _*)

lazy val sharedJVM = shared.jvm
lazy val sharedJS = shared.js

lazy val commonSettings = Seq(
  scalaVersion := "2.12.4",

  scalacOptions := Seq(
    "-deprecation",
    "-encoding", "utf-8",
    "-explaintypes",
    "-feature",
    "-language:existentials",
    "-language:experimental.macros",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-unchecked",
    "-Xcheckinit",
    "-Xfatal-warnings",
    "-Xfuture",
    "-Xlint",
    "-Yno-adapted-args",
    "-Ypartial-unification",
    "-Ywarn-dead-code",
    "-Ywarn-extra-implicit",
    "-Ywarn-inaccessible",
    "-Ywarn-infer-any",
    "-Ywarn-nullary-override",
    "-Ywarn-nullary-unit",
    "-Ywarn-numeric-widen",
    "-Ywarn-unused",
    "-Ywarn-value-discard"
  ),
  scalacOptions in (Compile, console) ~= {
    _.filterNot { opt => opt.startsWith("-X") || opt.startsWith("-Y") }
  },
  scalacOptions in (Test, console) ~= {
    _.filterNot { opt => opt.startsWith("-X") || opt.startsWith("-Y") }
  }
)

// Loads the server project at sbt startup
onLoad in Global := (onLoad in Global).value andThen {s: State => "project server" :: s}
