addSbtPlugin("com.vmunier"      % "sbt-web-scalajs"     % "1.0.6")
addSbtPlugin("io.spray"         % "sbt-revolver"        % "0.9.1")
addSbtPlugin("org.scala-js"     % "sbt-scalajs"         % "0.6.19")
