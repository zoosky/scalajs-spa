package spa.server

import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, StatusCodes }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route

object Router {
  val route: Route =
    (get & pathEndOrSingleSlash)(index) ~
      (get & path("email" / Segment / "validate"))(validateEmail) ~
      path("assets" / Remaining) { file =>
        encodeResponse {
          getFromResource(s"public/$file")
        }
      }

  private val index =
    complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, Index().render))

  private def validateEmail(email: String) = {
    if (email.contains("@"))
      complete(StatusCodes.OK)
    else
      complete(StatusCodes.BadRequest)
  }
}
