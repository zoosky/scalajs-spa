package spa.server

import scalatags.Text.all._
import scalatags.text.Frag
import scalatags.Text.tags2

object Index {
  def apply(): Frag =
    html(
      head(
        meta(charset := "utf-8"),
        meta(name := "viewport", content := "width=device-width, initial-scale=1"),
        tags2.title("monadic-html"),
        link(rel := "stylesheet", href := "assets/reset.css")
      ),
      body(
        div(
          script(src := "assets/client-fastopt.js")
        )
      )
    )
}
