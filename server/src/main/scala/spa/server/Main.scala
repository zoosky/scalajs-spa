package spa.server

import scala.concurrent.Await
import scala.concurrent.duration._
import scala.util.{ Failure, Success }

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory

object Main {
  val logger = LoggerFactory.getLogger(getClass)

  def main(args: Array[String]): Unit = {
    implicit val system = ActorSystem(s"actor-system")
    implicit val materializer = ActorMaterializer()
    implicit val executionContext = system.dispatcher

    val config = ConfigFactory.load()
    val interface = config.getString("http.interface")
    val port = config.getInt("http.port")

    Await
      .ready(Http().bindAndHandle(Router.route, interface, port), 10.seconds)
      .onComplete {
        case Success(server) =>
          logger.info(s"Server is running at http://$interface:$port")
          addShutdownHook(server)
        case Failure(error) =>
          logger.error(error.toString)
      }
  }

  private def addShutdownHook(server: ServerBinding)(implicit system: ActorSystem): Unit =
    Runtime.getRuntime.addShutdownHook(new Thread() {
      override def run(): Unit = {
        logger.info(s"Stopping server…")
        server.unbind()
        system.terminate()
        ()
      }
    })
}
