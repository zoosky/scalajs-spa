# Scala.js SPA

Single page application with Scala.js.

## Dependencies

- `akka-http`
- `monadic-html`
- `scalacss`
- `scalatags`

## Getting started

```bash
sbt ~reStart
```
