package spa.client.component

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

import mhtml._
import scala.scalajs.js.Date
import scala.scalajs.js.timers.setTimeout
import xml.Node

import spa.client.Icon

object ActionButton {
  def apply(action: () => Future[_]): Node = {
    val loading: Var[Boolean] = Var(false)

    def click(): Unit = {
      loading.update(_ => true)
      val time = new Date().getTime()

      action().andThen { case _ =>
        val elapsedTime = new Date().getTime() - time
        setTimeout(Math.max(0, 500 - elapsedTime)) {
          loading.update(_ => false)
        }
      }

      ()
    }

    <button onclick={ () => click() }>
      { loading.map(l => if (l) Icon.loading else <span>loading</span>) }
    </button>
  }
}
