package spa.client

import mhtml._
import org.scalajs.dom

import spa.client.view.App

object Main {
  def main(args: Array[String]): Unit = {
    mount(dom.document.body, App())
    ()
  }
}
