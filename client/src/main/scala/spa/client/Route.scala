package spa.client

import mhtml._
import org.scalajs.dom.raw.PopStateEvent
import org.scalajs.dom.{ window, MouseEvent }
import scala.scalajs.js.URIUtils

sealed trait Route

object Route {
  case object Home extends Route
  case object Counters extends Route
  case object Progress extends Route

  val current: Var[Route] = Var(parse(window.location.hash))

  window.onpopstate = (e: PopStateEvent) => {
    current.update(_ => parse(window.location.hash))
  }

  // Prevent page changes when clicking with the mouse left button
  window.onclick = { (event: MouseEvent) =>
    if (event.button == 0) event.preventDefault()
  }

  def parse(hash: String): Route =
    pathAndParams(hash) match {
      case ("counters" :: Nil, _) => Counters
      case ("progress" :: Nil, _) => Progress
      case _ => Home
    }

  def pathAndParams(hash: String): (List[String], List[String]) = {
    def splitPath(path: String) = path.split("/").drop(1).toList
    URIUtils.decodeURI(hash.drop(1)).split('?') match {
      case Array(path) => (splitPath(path), Nil)
      case Array(path, params) => (splitPath(path), params.split("&").toList)
    }
  }

  def url(route: Route): String = {
    val hash = route match {
      case Home => "/"
      case Counters => "/counters"
      case Progress => "/progress"
    }
    window.location.origin + window.location.pathname + "#" + URIUtils.encodeURI(hash)
  }

  def goTo(route: Route): Unit = {
    push(route)
    current.update(_ => route)
  }

  def push(route: Route): Unit =
    window.history.pushState(null, "", url(route));
}

object Param {
  def apply(key: String, value: String): (String, String) = (key, value)

  def unapply(x: Any): Option[(String, String)] =
    x match {
      case str: String =>
        str.split("=") match {
          case Array(key, value) => Some((key, value))
          case _ => None
        }
      case _ => None
    }

  def format(params: Seq[(String, String)]): String =
    if (params.isEmpty)
      ""
    else
      params
        .map { case (key, value) => s"$key=$value" }
        .mkString("?", "&", "")
}
