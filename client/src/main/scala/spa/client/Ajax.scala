package spa.client

import scala.concurrent.Future
import scala.util.{Try, Success, Failure}

import org.scalajs.dom.ext.{ Ajax => ScalajsAjax }
import play.api.libs.json.{Json, Reads}
import scala.concurrent.ExecutionContext.Implicits.global

sealed trait AjaxResult[T]

object Ajax {
  def getValue[T: Reads](path: String): Future[Either[String, T]] =
    get(path).map {
      case Left(error) =>
        Left(error)
      case Right(str) =>
        Try(Json.parse(str).as[T]) match {
          case Failure(_) =>
            Left(s"Read error while fetching $path")
          case Success(x) =>
            Right(x)
        }
    }

  def get(path: String): Future[Either[String, String]] =
    ScalajsAjax
      .get(path)
      .map { response =>
        if (isOkStatus(response.status))
          Right(response.responseText)
        else
          Left(s"Status error ${response.status} while fetching $path")
      }
      .recover { case _ =>
        Left(s"HTTP error while fetching $path")
      }

  private def isOkStatus(status: Int): Boolean =
    status >= 200 && status < 300 || status == 304
}
