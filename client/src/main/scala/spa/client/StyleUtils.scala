package spa.client

import scalacss.DevDefaults._
import scalacss.internal.Env
import xml.Node

object StyleUtils {
  def render(style: StyleSheet.Inline): Node = {
    <style>{ style.render(cssStringRenderer, implicitly[Env]) }</style>
  }
}
