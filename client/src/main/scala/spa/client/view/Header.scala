package spa.client.view

import scalacss.DevDefaults._
import xml.Node

import spa.client.StyleUtils

object Header {
  def apply(): Node =
    <div>
      { StyleUtils.render(Style) }
      <header class={ Style.header.htmlClass }>
        Monadic-html
      </header>
    </div>

  private object Style extends StyleSheet.Inline {
    import dsl._

    val header = style(
      backgroundColor.darkblue,
      color.white,
      fontSize(25.px),
      fontWeight.bold,
      height(60.px),
      lineHeight(60.px)
    )
  }
}
