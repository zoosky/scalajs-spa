package spa.client.view

import scalacss.DevDefaults._
import xml.Node

import spa.client.{ StyleUtils, Route }

object Menu {
  def apply(): Node =
    <div>
      { StyleUtils.render(Style) }
      <ul class={ Style.menu.htmlClass }>
        <li class={ Style.section.htmlClass } onclick={ () => Route.goTo(Route.Home) }>Home</li>
        <li class={ Style.section.htmlClass } onclick={ () => Route.goTo(Route.Counters) }>Counters</li>
        <li class={ Style.section.htmlClass } onclick={ () => Route.goTo(Route.Progress) }>Progress</li>
      </ul>
    </div>

  private object Style extends StyleSheet.Inline {
    import dsl._

    val menu = style(
    )

    val section = style(
      padding(20.px),
      color.darkblue,
      fontWeight.bold,
      cursor.pointer,
      &.hover(color.blue)
    )
  }
}
