package spa.client.view.page

import scala.scalajs.js.Dynamic
import scalacss.DevDefaults._
import xml.Node

import mhtml._
import spa.client.component.ActionButton
import spa.client.{ StyleUtils, Ajax }

object Progress {
  def apply(): Node = {
    val email: Var[String] = Var("")

    <div>
      { StyleUtils.render(Style) }

      <input
        type="text"
        placeholder="email"
        oninput={ (event: Dynamic) => email.update(_ => event.target.value.asInstanceOf[String]) }
      />

      { email.map(e => ActionButton(() => Ajax.get(s"email/$email/validate"))) }
    </div>
  }

  private object Style extends StyleSheet.Inline {
    import dsl._

    val email = style(
      marginBottom(1.em)
    )
  }
}
