package spa.client.view.page

import java.util.UUID

import mhtml._
import xml.Node

case class Count private (
  id: UUID,
  value: Var[Int]
)

object Count {
  def empty: Count = Count(UUID.randomUUID(), Var(0))
}

object Counters {
  def apply(): Node = {
    val counts = Var(Seq.fill(5)(Count.empty))

    def remove(id: UUID): Unit = counts.update(_.filter(_.id != id))

    <div>
      <h2>{ counts.map(_.length) } counters</h2>
      <button onclick={ () => counts.update(Count.empty +: _) }>Add a counter</button>
      { counts.map(_.map(count => Counter(count.value, remove(count.id)))) }
    </div>
  }
}

object Counter {
  def apply(count: Var[Int], remove: => Unit): Node =
    <div>
      <br />
      <button onclick={ () => count.update(_ - 1) }>-</button>
      { count }
      <button onclick={ () => count.update(_ + 1) }>+</button>
      <br />
      <button onclick={ () => remove }>remove</button>
      <br />
    </div>
}
