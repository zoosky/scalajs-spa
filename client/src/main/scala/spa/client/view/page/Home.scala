package spa.client.view.page

import scalacss.DevDefaults._
import xml.Node

import spa.client.StyleUtils

object Home {
  def apply(): Node =
    <div>
      { StyleUtils.render(Style) }

      <p class={ Style.paragraph.htmlClass }>
        Orci. Donec non nisl. Mauris lacus sapien, congue a, facilisis at,
        egestas vel, quam. Vestibulum ante ipsum primis in faucibus orci luctus
        et ultrices posuere cubilia Curae.
      </p>

      <p class={ Style.paragraph.htmlClass }>
        Phasellus ipsum odio, suscipit nec, fringilla at, vehicula quis, tellus.
        Phasellus gravida condimentum dui. Aenean imperdiet arcu vitae ipsum.
        Duis dapibus, nisi non porttitor iaculis, ligula odio sollicitudin
        mauris, non luctus nunc massa a velit. Fusce ac nisi. Integer volutpat
        elementum metus. Vivamus luctus ultricies diam. Curabitur euismod.
        Vivamus quam. Nunc ante. Nulla mi nulla, vehicula nec, ultrices a,
        tincidunt vel, enim.
      </p>

      <p class={ Style.paragraph.htmlClass }>
        Suspendisse potenti. Aenean sed velit. Nunc a urna quis turpis imperdiet
        sollicitudin. Mauris aliquam.
      </p>
    </div>

  private object Style extends StyleSheet.Inline {
    import dsl._

    val paragraph = style(
      width(500.px),
      marginBottom(1.em),
      textAlign.justify
    )
  }
}
