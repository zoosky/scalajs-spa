package spa.client.view

import scalacss.DevDefaults._
import xml.Node

import spa.client.view.page.{ Home, Counters, Progress }
import spa.client.{ Route, StyleUtils }

object App {
  def apply(): Node =
    <div>
      { StyleUtils.render(Style) }
      { Header() }
      <div class={ Style.menuAndMain.htmlClass }>
        { Menu() }
        <div class={ Style.main.htmlClass }>
          { Route.current.map {
              case Route.Home => Home()
              case Route.Counters => Counters()
              case Route.Progress => Progress()
          } }
        </div>
      </div>
    </div>

  private object Style extends StyleSheet.Inline {
    import dsl._

    val menuAndMain = style(
      display.flex,
      flexDirection.row
    )

    val main = style(
      margin(10.px)
    )
  }
}
